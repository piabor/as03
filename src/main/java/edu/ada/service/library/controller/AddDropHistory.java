package edu.ada.service.library.controller;

import edu.ada.service.library.model.entity.AddDropHistoryEntity;

import java.util.List;

public interface AddDropHistory {

    public List<AddDropHistoryEntity> userHistory(String token);

}
