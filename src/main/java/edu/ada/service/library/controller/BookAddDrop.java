package edu.ada.service.library.controller;

import org.springframework.http.ResponseEntity;

public interface BookAddDrop {

    public ResponseEntity addDropBooks(String add, String drop, String token);

}
