package edu.ada.service.library.controller.impl;

import edu.ada.service.library.controller.AuthenticationWS;
import edu.ada.service.library.model.dto.RegistrationModel;
import edu.ada.service.library.service.impl.AuthenticationServiceImpl;
import edu.ada.service.library.service.JwtService;
import edu.ada.service.library.utils.PasswordEncryption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
public class AuthenticationWSImpl implements AuthenticationWS {

    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSImpl.class);

    @Autowired
    private AuthenticationServiceImpl authenticationService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtService jwtService;

    @Override
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ResponseEntity createToken(
            @RequestHeader("email") String email,
            @RequestHeader("password") String password
    ) {
        try{
            UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(email, PasswordEncryption.hashPassword(password));
            authenticationManager.authenticate(authReq);
            final UserDetails userEmail = authenticationService.loadUserByUsername(email);
            final String jwt = jwtService.generateToken(userEmail);

            log.info("Token created successfully");
            return ResponseEntity.ok("Your token for authentication: " + jwt);
        } catch (BadCredentialsException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.ok("Email or password is wrong");
        }

    }


    @Override
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity register(
            @RequestBody RegistrationModel formData
    ) {
        int result = authenticationService.registration(formData);
        if(result == -2){
            log.warn("Password validation warning");
            return ResponseEntity.ok("Passwords should be 8-20 long and contain at least one number, one upper case and one lower case letter.");
        } else if (result == -1){
            log.warn("Invalid email address");
            return ResponseEntity.ok("Invalid email address!");
        } else if (result == -3){
            log.warn("Non-text or blank input for first name and/or last name");
            return ResponseEntity.ok("You cannot leave first and last name field blank and cannot enter symbols or numbers.");
        } else if (result == -4){
            log.warn("A used email address given");
            return ResponseEntity.ok("This email address is already being used.");
        } else{
            log.info("User registered");
            return ResponseEntity.ok("You have been successfully registered!");
        }
    }

}
