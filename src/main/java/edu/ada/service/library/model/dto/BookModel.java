package edu.ada.service.library.model.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BookModel implements Serializable {

    private String title;
    private String author;
    private String description;
    private Date publishDate;
    private String genre;
    private String availability;
}
