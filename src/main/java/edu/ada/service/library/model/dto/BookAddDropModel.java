package edu.ada.service.library.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BookAddDropModel implements Serializable {
    private Long userId;
    private Long bookId;
}
