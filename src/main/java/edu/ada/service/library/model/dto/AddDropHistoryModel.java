package edu.ada.service.library.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AddDropHistoryModel implements Serializable {
    private Long userId;
    private Long bookId;
    private String addDropStatus;
    private String bookTitle;
    private Date recordDate;
}
