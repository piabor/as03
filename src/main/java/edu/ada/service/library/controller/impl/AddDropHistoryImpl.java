package edu.ada.service.library.controller.impl;

import edu.ada.service.library.controller.AddDropHistory;
import edu.ada.service.library.model.entity.AddDropHistoryEntity;
import edu.ada.service.library.model.entity.UserEntity;
import edu.ada.service.library.model.repository.AddDropHistoryRepository;
import edu.ada.service.library.model.repository.UserRepository;
import edu.ada.service.library.service.JwtService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class AddDropHistoryImpl implements AddDropHistory {

    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSImpl.class);

    @Autowired
    private AddDropHistoryRepository addDropHistoryRepository;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserRepository userRepository;

    @Override
    @RequestMapping(value = "/history", method = RequestMethod.GET)
    public List<AddDropHistoryEntity> userHistory(
            @RequestHeader(value = "token") String token
    ) {
        try {
            UserEntity currentUser = userRepository.findFirstByEmail(jwtService.extractUsername(token));
            List<AddDropHistoryEntity> history = addDropHistoryRepository.findByOrderByRecordDateDesc();
            for (int counter = history.size() - 1; counter >= 0; counter--) {
                if (!history.get(counter).getUserId().equals(currentUser.getId())) {
                    history.remove(counter);
                }
            }
            return history;
        } catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
