package edu.ada.service.library.controller;

import edu.ada.service.library.model.entity.BookEntity;

import java.util.List;

public interface Search {

    List<BookEntity> searchBooks(String title, String author, String genre);

}
