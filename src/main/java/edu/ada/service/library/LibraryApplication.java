package edu.ada.service.library;

import edu.ada.service.library.model.dto.BookModel;
import edu.ada.service.library.model.entity.BookEntity;
import edu.ada.service.library.model.repository.BookRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.*;

@SpringBootApplication
public class LibraryApplication {

	public static void main(String[] args) { SpringApplication.run(LibraryApplication.class, args); }

	@Bean
	CommandLineRunner commandLineRunnerBook(BookRepository bookRepository) {
		return args -> {
			List<BookEntity> bookList = new ArrayList<>();
			bookList.add(new BookEntity(new BookModel(
					"The Lord Of The Rings",
					"John Ronald Reuel Tolkien",
					"Tolkien's seminal three-volume epic chronicles the War of the Ring, in which Frodo the hobbit and his companions set out to destroy the evil Ring of Power and restore peace to Middle-earth. The beloved trilogy still casts a long shadow, having established some of the most familiar and enduring tropes in fantasy literature.",
					new GregorianCalendar(1954, Calendar.JULY, 29).getTime(),
					"Sci-Fi",
					"Available"
			)));
			bookList.add(new BookEntity(new BookModel(
					"Ender's Game",
					"Orson Scott Card",
					"Young Andrew \"Ender\" Wiggin, bred to be a genius, is drafted to Battle School where he trains to lead the century-long fight against the alien Buggers.",
					new GregorianCalendar(1985, Calendar.JANUARY, 15).getTime(),
					"Sci-Fi",
					"Available"
			)));

			bookList.add(new BookEntity(new BookModel(
					"Post-Capitalist Society",
					"Peter F. Drucker",
					"Well known for predicting future events such as the economic rise of Japan and the emergence of an information society, this book argues that First World nations have already moved to a society beyond capitalism, in that capital is owned by organisations rather than individuals. Regular citizens therefore become, in essence, the owners of enterprises, and therefore the owners of capital, meaning capitalism is changed without being destroyed. Drucker concludes by arguing that organisations will continue to become highly specialised, and that outsourcing rather than diversification will define the future.",
					new GregorianCalendar(1992, Calendar.DECEMBER, 23).getTime(),
					"Economy",
					"Available"
			)));
			bookList.add(new BookEntity(new BookModel(
					"Freakonomics: A Rogue Economist Explores the Hidden Side of Everything",
					"Steven D. Levitt & Stephen J. Dubner",
					"Freakonomics deliberately blends pop culture topics with the theories of economics by discussing specific amusing or interesting examples across a series of articles written by the authors – for example, how cheating operates in different businesses including teaching, sumo wrestling, and bagel selling. The light tone and fun content have made this book a big hit with the public, and it has stayed on bestseller lists since it was first published in 2005. It has sometimes faced criticism for being more a work of sociology or even criminology than economics; however, it remained popular, and the authors started their own blog, also entitled Freakonomics, in 2005.",
					new GregorianCalendar(2005, Calendar.APRIL, 12).getTime(),
					"Economy",
					"Available"
			)));

			bookList.add(new BookEntity(new BookModel(
					"A Brief History of Time",
					"Stephen Hawking",
					"A Brief History of Time: From the Big Bang to Black Holes is a theoretical book on cosmology by English physicist Stephen Hawking. It was first published in 1988. Hawking wrote the book for readers who had no prior knowledge of physics and people who are interested in learning something new about interesting subjects.",
					new GregorianCalendar(1988, Calendar.JANUARY, 1).getTime(),
					"STEM",
					"Available"
			)));
			bookList.add(new BookEntity(new BookModel(
					"Astrophysics for People in a Hurry",
					"Neil deGrasse Tyson",
					"What is the nature of space and time? How do we fit within the universe? How does the universe fit within us? There’s no better guide through these mind-expanding questions than acclaimed astrophysicist and best-selling author Neil deGrasse Tyson.",
					new GregorianCalendar(2017, Calendar.MAY, 2).getTime(),
					"STEM",
					"Available"
			)));

			bookList.add(new BookEntity(new BookModel(
					"A History of Western Philosophy",
					"Bertrand Russell",
					"Bertrand Russel’s ‘A History of Western Philosophy’ has held a position of reverence since it’s first publication in 1945. Comprehensive, erudite and revealing; this is a history written by one who would go on to become a regarded philosopher in his own right.",
					new GregorianCalendar(1967, Calendar.OCTOBER, 30).getTime(),
					"Philosophy",
					"Available"
			)));
			bookList.add(new BookEntity(new BookModel(
					"The Essential Epicurus",
					"Epicurus",
					"Epicureanism is commonly regarded as the refined satisfaction of physical desires. Epicurus  did not stand for eating too much or too much sex, but for loving life and preserving that love with the smaller delights to be found daily. As a philosophy, however, it also denoted the striving after an independent state of mind and body, imperturbability, and reliance on sensory data as the true basis of knowledge.",
					new GregorianCalendar(1993, Calendar.MAY, 1).getTime(),
					"Philosophy",
					"Available"
			)));

			bookRepository.saveAll(bookList);
		};
	}
}

