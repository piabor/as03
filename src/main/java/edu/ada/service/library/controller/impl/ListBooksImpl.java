package edu.ada.service.library.controller.impl;

import edu.ada.service.library.controller.ListBooks;
import edu.ada.service.library.model.entity.BookEntity;
import edu.ada.service.library.model.repository.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ListBooksImpl implements ListBooks {

    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSImpl.class);

    @Autowired
    private BookRepository bookRepository;

    @Override
    @RequestMapping({"/list-books"})
    public List<BookEntity> listPage() {
        try {
            List<BookEntity> bookEntityList = (List<BookEntity>) bookRepository.findAll();
            log.info("All books are listed");
            return bookEntityList;
        } catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
