package edu.ada.service.library.controller.impl;

import edu.ada.service.library.controller.CurrentlyPickedUp;
import edu.ada.service.library.model.entity.BookAddDropEntity;
import edu.ada.service.library.model.entity.BookEntity;
import edu.ada.service.library.model.entity.UserEntity;
import edu.ada.service.library.model.repository.BookAddDropRepository;
import edu.ada.service.library.model.repository.BookRepository;
import edu.ada.service.library.model.repository.UserRepository;
import edu.ada.service.library.service.JwtService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CurrentlyPickedUpImpl implements CurrentlyPickedUp {

    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSImpl.class);

    @Autowired
    UserRepository userRepository;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    BookAddDropRepository bookAddDropRepository;

    @Autowired
    JwtService jwtService;

    @Override
    @RequestMapping(value = "/mybooks", method = RequestMethod.GET)
    public List<BookEntity> myCurrentBooks(
            @RequestHeader(value = "token") String token
    ) {
        try {
            UserEntity currentUser = userRepository.findFirstByEmail(jwtService.extractUsername(token));
            List<BookEntity> myBookList = new ArrayList<>();
            List<BookAddDropEntity> currentBooks = bookAddDropRepository.findAllByUserId(currentUser.getId());
            if (currentBooks != null) {
                for (BookAddDropEntity currentBook : currentBooks) {
                    myBookList.add(bookRepository.findFirstById(currentBook.getBookId()));
                }
                return myBookList;
            } else {
                log.warn("User has not picked up any book");
            }
        } catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
