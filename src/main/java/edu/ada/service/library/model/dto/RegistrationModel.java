package edu.ada.service.library.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class RegistrationModel implements Serializable {

    private String firstName;
    private String lastName;
    private String email;
    private String birthDay;
    private String password;
}
