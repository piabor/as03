package edu.ada.service.library.controller.impl;

import edu.ada.service.library.controller.BookAddDrop;
import edu.ada.service.library.model.dto.AddDropHistoryModel;
import edu.ada.service.library.model.dto.BookAddDropModel;
import edu.ada.service.library.model.entity.AddDropHistoryEntity;
import edu.ada.service.library.model.entity.BookAddDropEntity;
import edu.ada.service.library.model.entity.BookEntity;
import edu.ada.service.library.model.entity.UserEntity;
import edu.ada.service.library.model.repository.AddDropHistoryRepository;
import edu.ada.service.library.model.repository.BookAddDropRepository;
import edu.ada.service.library.model.repository.BookRepository;
import edu.ada.service.library.model.repository.UserRepository;
import edu.ada.service.library.service.JwtService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class BookAddDropImpl implements BookAddDrop {

    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSImpl.class);

    @Autowired
    private BookAddDropRepository bookAddDropRepository;

    @Autowired
    private AddDropHistoryRepository addDropHistoryRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtService jwtService;

    @Override
    @RequestMapping(value = "/add-drop", method = RequestMethod.GET)
    public ResponseEntity addDropBooks(
            @RequestHeader(value = "add", defaultValue = "3mpty4dd") String add,
            @RequestHeader(value = "drop", defaultValue = "3mptyDr0p") String drop,
            @RequestHeader(value = "token") String token
    ) {
        try {
            UserEntity currentUser = userRepository.findFirstByEmail(jwtService.extractUsername(token));
            if (!add.equals("3mpty4dd") && drop.equals("3mptyDr0p")) {
                Long addBookId = Long.parseLong(add);
                BookEntity addedBook = bookRepository.findFirstById(addBookId);
                if (addedBook != null) {
                    if (addedBook.getAvailability().equals("Available")) {
                        bookRepository.changeAvailability("Unavailable! Taken by: " + currentUser.getFirstName() + " " + currentUser.getLastName(), addBookId);
                        bookAddDropRepository.save(new BookAddDropEntity(new BookAddDropModel(
                                currentUser.getId(),
                                addBookId
                        )));
                        addDropHistoryRepository.save(new AddDropHistoryEntity(new AddDropHistoryModel(
                                currentUser.getId(),
                                addBookId,
                                "Picked up",
                                addedBook.getTitle(),
                                new Date()
                        )));
                        log.info(currentUser.getEmail() + " picked up a book");
                        return ResponseEntity.ok("You have successfully picked up the book '" + addedBook.getTitle() + "'.");
                    } else if (bookAddDropRepository.findFirstByBookId(addBookId).getUserId().equals(currentUser.getId())) {
                        log.warn("The user already picked up the book");
                        return ResponseEntity.ok("You have already picked up this book!");
                    } else {
                        UserEntity currentBookUser = userRepository.findFirstById(bookAddDropRepository.findFirstByBookId(addBookId).getUserId());
                        log.warn("Unavailable book is requested by a user");
                        return ResponseEntity.ok("Book is unavailable and currently picked up by " + currentBookUser.getFirstName() + " " + currentBookUser.getLastName() + ".");
                    }

                } else {
                    log.error("Book with the given id is not found");
                    return ResponseEntity.ok("Book Not Found!");
                }
            } else if (!drop.equals("3mptyDr0p") && add.equals("3mpty4dd")) {
                Long dropBookId = Long.parseLong(drop);
                BookAddDropEntity bookAddDropEntity = bookAddDropRepository.findFirstByUserIdAndBookId(currentUser.getId(), dropBookId);
                if (bookAddDropEntity != null) {
                    bookRepository.changeAvailability("Available", dropBookId);
                    bookAddDropRepository.delete(bookAddDropEntity);

                    addDropHistoryRepository.save(new AddDropHistoryEntity(new AddDropHistoryModel(
                            currentUser.getId(),
                            dropBookId,
                            "Dropped off",
                            bookRepository.findFirstById(dropBookId).getTitle(),
                            new Date()
                    )));
                    log.info("A user dropped off a book");
                    return ResponseEntity.ok("You have successfully dropped off the book.");
                } else {
                    log.warn("A user tries to drop a book they did not pick up");
                    return ResponseEntity.ok("This book is not found in your currently taken books list.");
                }
            } else if (!add.equals("3mpty4dd") && !drop.equals("3mptyDr0p")) {
                log.warn("Add/drop parameters cannot be used together");
                return ResponseEntity.ok("You cannot add and drop the books at the same time.");
            }

            log.warn("Add/Drop parameter not given");
            return ResponseEntity.ok("Add/Drop parameter is not given!");
        } catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
