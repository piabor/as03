package edu.ada.service.library.controller.impl;

import edu.ada.service.library.controller.Search;
import edu.ada.service.library.model.entity.BookEntity;
import edu.ada.service.library.model.repository.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SearchImpl implements Search {

    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSImpl.class);

    @Autowired
    private BookRepository bookRepository;

    @Override
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public List<BookEntity> searchBooks(
            @RequestHeader(value = "title", defaultValue = "3mptyT1tl3") String title,
            @RequestHeader(value = "author", defaultValue = "3mpty4uth0r") String author,
            @RequestHeader(value = "genre", defaultValue = "3mptyG3nr3") String genre
    ) {
        try {
            List<BookEntity> foundBooks = (List<BookEntity>) bookRepository.findAll();
            for (int counter = foundBooks.size() - 1; counter >= 0; counter--) {
                if ((!foundBooks.get(counter).getAuthor().toLowerCase().contains(author.toLowerCase()) || author.equals("3mpty4uth0r")) && (!foundBooks.get(counter).getTitle().toLowerCase().contains(title.toLowerCase()) || title.equals("3mptyT1tl3")) && (!foundBooks.get(counter).getGenre().toLowerCase().contains(genre.toLowerCase()) || genre.equals("3mptyG3nr3"))) {
                    foundBooks.remove(counter);
                }
            }
            if (foundBooks.size() > 0) {
                log.info("Search successful");
                return foundBooks;
            } else {
                log.warn("Search has not given any result");
            }
        } catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
