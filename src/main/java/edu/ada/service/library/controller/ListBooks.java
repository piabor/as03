package edu.ada.service.library.controller;

import edu.ada.service.library.model.entity.BookEntity;

import java.util.List;

public interface ListBooks {

    List<BookEntity> listPage();

}
