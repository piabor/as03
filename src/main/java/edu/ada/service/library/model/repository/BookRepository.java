package edu.ada.service.library.model.repository;

import edu.ada.service.library.model.entity.BookEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface BookRepository extends CrudRepository<BookEntity, Long> {
    BookEntity findFirstById(Long id);

    @Transactional
    @Modifying
    @Query("UPDATE BookEntity b SET b.availability =:status WHERE b.id =:id")
    void changeAvailability(@Param("status") String status, @Param("id") Long id);
}
