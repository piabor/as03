package edu.ada.service.library.model.entity;

import edu.ada.service.library.model.dto.BookModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.SEQUENCE;

@NoArgsConstructor
@Data
@Entity
@Table(name = "books")
public class BookEntity {
    @Id
    @SequenceGenerator(
            name = "book_sequence",
            sequenceName = "book_sequence",
            allocationSize = 1,
            initialValue = 20001
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "book_sequence"
    )
    @Column(
            name= "id",
            updatable = false
    )
    private Long id;

    private String title;
    private String author;
    @Column(
            name= "description",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String description;
    private Date publishDate;
    private String genre;
    private String availability;

    public BookEntity(BookModel bookModel) {
        this.title = bookModel.getTitle();
        this.author = bookModel.getAuthor();
        this.description = bookModel.getDescription();
        this.publishDate = bookModel.getPublishDate();
        this.genre = bookModel.getGenre();
        this.availability = bookModel.getAvailability();
    }
}
